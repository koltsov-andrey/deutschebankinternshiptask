package com.company;

import java.io.IOException;
import java.util.logging.*;


public class UserCommandsLogger {

    private static final Logger LOGGER = Logger.getLogger(UserCommandsLogger.class
            .getClass().getName());
    static Handler fileHandler = null;

    public static void setup() {
        try {
            fileHandler = new FileHandler("./UserActions.log"); // NOTE: Creating a file
            SimpleFormatter simple = new SimpleFormatter();
            fileHandler.setFormatter(simple);
            LOGGER.addHandler(fileHandler);
            LOGGER.setUseParentHandlers(false);
            LOGGER.setLevel(Level.INFO);
        } catch (IOException error) {
            System.err.println("ERROR: Log file couldn't be created. Reason: " + error.getMessage());
        }
    }

    public static void logCommand(String[] command) {
        String loggedCommand = String.join(" ", command);
        LOGGER.log(LoggerCommandLevel.COMMAND, loggedCommand);
    }
    public static void logCommand(String command) {
        LOGGER.log(LoggerCommandLevel.COMMAND, command);
    }
}

class LoggerCommandLevel extends Level {
    public static final Level COMMAND = new LoggerCommandLevel("USER: ", Level.INFO.intValue());

    public LoggerCommandLevel(String name, int value) {
        super(name, value);
    }
}


