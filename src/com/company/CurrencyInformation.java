package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CurrencyInformation {

    public static String idenifier;
    double priceRelatedToUSD;
    double currentPriceRelatedToUSD;
    double avaliableDeviationFromUSD;

    public CurrencyInformation(String idenifier, double priceRelatedToUSD, double avaliableDeviationFromUSD) {
        this.idenifier = idenifier;
        this.priceRelatedToUSD = priceRelatedToUSD;
        this.currentPriceRelatedToUSD = this.priceRelatedToUSD;
        this.avaliableDeviationFromUSD = avaliableDeviationFromUSD;
    }

    public  double getAvaliableDeviationFromUSD() {
        return this.avaliableDeviationFromUSD;
    }
    public double getCurrentPriceRelatedToUSD() {
        return currentPriceRelatedToUSD;
    }

    public void updatePrice(double newPrice) {
        this.currentPriceRelatedToUSD = newPrice;
    }
}
