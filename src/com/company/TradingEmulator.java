package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class TradingEmulator {

    public static Scanner inputScanner;

    public static HashMap<String, List<Double>> getCurrencyData() {
        return currencyData;
    }

    public static ArrayList<String> getCurrencyTags() {
        ArrayList<String> currencyMarks = new ArrayList<String>();
        currencyMarks.addAll(currencyData.keySet());
        return currencyMarks;
    }

    private static HashMap<String, List<Double>> currencyData;

    public static void main(String[] args) {
        if (args.length != Constants.AVALIABLE_ARGUMENTS_AMOUNT) {
            try {
                String misleadingMsg = "Incorrect input has been found. The program recives filepath."
                        + Constants.USAGE_EXAMPLE;
                throw new IllegalArgumentException(misleadingMsg);
            } catch (IllegalArgumentException error) {
                System.out.println("ERROR: " + error.getMessage());
            }
        }

        try {
            FileParser parser = new FileParser(args[0]);
            currencyData = parser.getDataFromFile();
        } catch (IOException error) {
            String reason = "";
            String misleadingMsg = "An error has occured while processing the file: ";
            if (error.getMessage().equals("null")) {
                reason = "file path is not correct";
                System.out.println(misleadingMsg + reason);
            } else {
                System.out.println(misleadingMsg + error.getMessage());
            }
        }  catch (IllegalArgumentException error) {
            System.out.println("File is not correct: " + error.getMessage());
        }

        inputScanner = new Scanner(System.in);
        CommandParser interactor = new CommandParser();
        UserCommandsLogger.setup();
        while (!CommandParser.isProgramTerminated()) {
            String enteredCommand = inputScanner.nextLine();
            try {
                interactor.handleCommand(enteredCommand);
            } catch (IllegalArgumentException error) {
                System.out.println(error.getMessage());
            } catch (InvalidCommandException error) {
                System.out.println(error.getMessage());
            } catch (InvalidCommandUsageException error) {
                System.out.println(error.getMessage());
            }
        }
    }
}
