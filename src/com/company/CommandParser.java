package com.company;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class CommandParser {

    private static boolean terminationFlag = false;
    private static HashMap<String, List<Double>> currencyData;
    private static ExchangeRatesPrinter exchangeRatesPrinter;
    private String currencyPair = "EURUSD";
    private boolean isRelatedCurrencyAssigned = false;

    static String relatedCurrency = Constants.CURRENCY_TAG_USG;
    static String currentCurrency = "RUB";
    static int amountOfPurchase = 0;
    static int ammountOfSelling = 0;

    public CommandParser() {
        this.terminationFlag = false;
        this.currencyData = TradingEmulator.getCurrencyData();
        File logFile = new File("/userActionsLog.log");
        SimpleFormatter formatter = new SimpleFormatter();
        System.out.println("\t Current currency pair: " + this.currencyPair);
    }

    public static boolean isProgramTerminated() {
        return terminationFlag;
    }

    private static void terminateProgram() {
        System.out.println("Quitting...");
        terminationFlag = true;
        System.exit(Constants.EXIT_SUCCESS);
    }

    // NOTE: Methods for handling "LIST" command

    private static void performCommandList() {
        ArrayList<String> currencyTags = TradingEmulator.getCurrencyTags();
        currencyTags.add(Constants.CURRENCY_TAG_USG);
        currencyTags = getPermuniation(currencyTags);
        for (int i = 0; i < currencyTags.size(); i++) {
            System.out.println(currencyTags.get(i));
        }
    }

    public static ArrayList<String> getPermuniation(ArrayList<String> currencies) {
        ArrayList<String> result = new ArrayList<String>();
        for (int i = 0; i < currencies.size(); ++i) {
            calculatePairs(i, currencies, result);
        }
        return result;
    }

    public static void calculatePairs(int elementNumber, ArrayList<String> currencies, ArrayList<String> result) {
        for (int i = 0; i < currencies.size(); ++i) {
            if (elementNumber == i) {
                continue;
            } else {
                String pair = (currencies.get(elementNumber) + currencies.get(i));
                result.add(pair);
            }
        }
    }

    // NOTE: Methods for handling "PRICE" command


    private void performCommandPrice(String[] commandInput) throws NullPointerException, IllegalArgumentException {
        String currencyPair = commandInput[1];
        String lhsCurrencyTag = currencyPair.substring(0, 3);
        String rhsCurrencyTag = currencyPair.substring(3);

        if (!currencyData.keySet().contains(lhsCurrencyTag)) {
            throw new IllegalArgumentException("Currency data hasn't been found for " + lhsCurrencyTag);
        } else if (!currencyData.keySet().contains(rhsCurrencyTag)) {
            throw new IllegalArgumentException("Currency data hasn't been found for " + rhsCurrencyTag);
        } else if (!currencyData.keySet().contains(lhsCurrencyTag) && !currencyData.keySet().contains(rhsCurrencyTag)) {
            throw new IllegalArgumentException("Currency data hasn't been found for " + lhsCurrencyTag +
                    " and " + rhsCurrencyTag);
        }

        if (lhsCurrencyTag.equals(rhsCurrencyTag)) {
            String misleadingMessage = "Equal currencies. The price is always be 1.0.";
            throw new IllegalArgumentException(misleadingMessage);
        }
        this.isRelatedCurrencyAssigned = true;
        currentCurrency = lhsCurrencyTag;
        relatedCurrency = rhsCurrencyTag;
        final List<Double> lhsCurrencyValues = new ArrayList<Double>(currencyData.get(lhsCurrencyTag));
        CurrencyInformation lhsCurrency = new CurrencyInformation(lhsCurrencyTag, lhsCurrencyValues.get(0), lhsCurrencyValues.get(1));

        final List<Double> rhsCurrencyValues = new ArrayList<Double>(currencyData.get(rhsCurrencyTag));
        CurrencyInformation rhsCurrency = new CurrencyInformation(rhsCurrencyTag, rhsCurrencyValues.get(0), rhsCurrencyValues.get(1));

        if (lhsCurrencyTag.equals(Constants.CURRENCY_TAG_USG)) {
            double lhsCurrencyValue = 1.0;
            double lhsCurrencyDeviation = 0.0;
            List<Double> lhsValues = Arrays.asList(lhsCurrencyValue, lhsCurrencyDeviation); // CORRECT; LHS TO RHS VAL
            currencyData.put(lhsCurrencyTag, lhsValues);
            CurrencyInformation usdInformation = new CurrencyInformation(Constants.CURRENCY_TAG_USG, lhsCurrencyValue, lhsCurrencyDeviation
            );
            lhsCurrency = usdInformation;
        } else if (rhsCurrencyTag.equals(Constants.CURRENCY_TAG_USG)) {
            double currentPrice = 1.0;
            double currentDeviation = 0.0;
            List<Double> usdValues = Arrays.asList(currentPrice, currentDeviation);
            currencyData.put(rhsCurrencyTag, usdValues);
            CurrencyInformation currencyInfo = new CurrencyInformation(lhsCurrencyTag, currentPrice, currentDeviation);
            rhsCurrency = currencyInfo;
        }



        double lhsToRhsPrice = rhsCurrency.getCurrentPriceRelatedToUSD() / lhsCurrency.getCurrentPriceRelatedToUSD();
        if (rhsCurrencyTag.equals(Constants.CURRENCY_TAG_USG)) {
            lhsToRhsPrice = 1 / lhsCurrency.getCurrentPriceRelatedToUSD();
        }

        double avaliableDeviation = lhsCurrency.getAvaliableDeviationFromUSD() + rhsCurrency.getAvaliableDeviationFromUSD() * lhsToRhsPrice;
        double priceLowerBound = lhsToRhsPrice - avaliableDeviation;
        double priceUpperBound = lhsToRhsPrice + avaliableDeviation;

        // TODO: ask if I should change only LHS currency value
        exchangeRatesPrinter = new ExchangeRatesPrinter(priceLowerBound, priceUpperBound, currencyData, lhsCurrencyTag, rhsCurrencyTag);
        exchangeRatesPrinter.start();

    }

    // NOTE: Methods for handling "STOP" command
    private static void performCommandStop() throws NullPointerException {
        String loggedCommand = "STOP";
        UserCommandsLogger.logCommand(loggedCommand);
        exchangeRatesPrinter.terminate();
    }


    // NOTE: Methods for handling "BUY" command:
    private static boolean isCurrencyDataExists(String currencyTag) {
        return (currencyData.get(currencyTag) != null);
    }

    public static double getPriceOfCurrencyRelatedToUSD(String currencyTag) {
        int currencyValueIndexPosition = 0;
        return ((currencyData.get(currencyTag)).get(currencyValueIndexPosition));
    }

    private void performTradingCommand(String[] commandInput) throws NullPointerException {
        if (!this.isRelatedCurrencyAssigned) {
            System.out.println("Currency pair wasn't set. Setting default: " + this.relatedCurrency);
        }
        currentCurrency = commandInput[2];
        if (!isCurrencyDataExists(currentCurrency)) {
            throw new NullPointerException("Currency data hasn't been found for both " + currentCurrency);
        }
        amountOfPurchase = Integer.parseInt(commandInput[1]);
        // USER BUYS <amount> <currency> SELLS <counter_amount> <counter_currency>

        String commandType = commandInput[0];
        String currencyPair = currentCurrency + relatedCurrency;
        String whiteSpace = " ";
        String[] argument = {whiteSpace, currencyPair};
        if (commandType.equals(Constants.COMMAND_BUY)) {
            UserCommandsLogger.logCommand(commandInput);
            ExchangeRatesPrinter.isBuying = true;
            performCommandPrice(argument);
        } else if (commandType.equals(Constants.COMMAND_SELL)) {
            UserCommandsLogger.logCommand(commandInput);
            ExchangeRatesPrinter.isSelling = true;
            performCommandPrice(argument);

        }
    }

    private static void processHelp(String answer) {
        answer = answer.toUpperCase();
        if (answer.equals(Constants.ANSWER_YES)) {
            exchangeRatesPrinter.terminate();
            System.out.println("Value updating paused.");
        } else if (answer.equals(Constants.ANSWER_NO)) {
            System.out.println("Continue...");
            exchangeRatesPrinter.activate();
        }
    }

    public void handleCommand(String enteredCommand) throws IllegalArgumentException,
            InvalidCommandException, InvalidCommandUsageException {
        enteredCommand = enteredCommand.toUpperCase(); // NOTE: making command case insensitive
        String expressionForParsingStrings = "\\s+";
        String[] commandInput = enteredCommand.split(expressionForParsingStrings);
        String command = commandInput[0];

        if (ExchangeRatesPrinter.isActive && (enteredCommand != null)) {
            // NOTE: handle situation when input is tough due to the speed of updating currency values from Excector
            exchangeRatesPrinter.pause();
            if (ExchangeRatesPrinter.isBuying && (enteredCommand.equals(Constants.CONFIRM_BUYING))) {
                double transactionValue = Double.parseDouble(String.format("%.3f", CommandParser.amountOfPurchase * exchangeRatesPrinter.getCurrentPrice()));
                String whiteSpace = " ";
                String logComand = "USER BUYS " + whiteSpace + CommandParser.amountOfPurchase + whiteSpace + CommandParser.currentCurrency + " SELLS " +
                        transactionValue + whiteSpace + CommandParser.relatedCurrency;
                System.out.println(logComand);
                String[] loggedData = {logComand, whiteSpace};
                UserCommandsLogger.logCommand(loggedData);
                exchangeRatesPrinter.terminate();
                ExchangeRatesPrinter.isBuying = false;
            } else if ((ExchangeRatesPrinter.isSelling) && enteredCommand.equals(Constants.CONFIRM_SELLING)) {
                double transactionValue = Double.parseDouble(String.format("%.3f", CommandParser.amountOfPurchase * (1 / exchangeRatesPrinter.getCurrentPrice())));
                String whiteSpace = " ";
                String logComand = "USER SELLS " + whiteSpace + CommandParser.amountOfPurchase + whiteSpace + CommandParser.relatedCurrency + " BUYS " +
                        transactionValue + whiteSpace + CommandParser.currentCurrency;
                System.out.println(logComand);
                String[] loggedData = {logComand, whiteSpace};
                UserCommandsLogger.logCommand(loggedData);
                exchangeRatesPrinter.terminate();
                ExchangeRatesPrinter.isSelling = false;
            } else {
                String askIfContinue = "Do you want to pause the printing of values? [y/n] ";
                System.out.print(askIfContinue);
                boolean isAnswerCorrect = false;
                while (!isAnswerCorrect) {
                    String enteredAnswer = TradingEmulator.inputScanner.nextLine();
                    String[] answer = enteredAnswer.split(expressionForParsingStrings);
                    if (answer[answer.length - 1].toUpperCase().equals(Constants.ANSWER_YES) || answer[answer.length - 1].toUpperCase().equals(Constants.ANSWER_NO)) {
                        isAnswerCorrect = true;
                        processHelp(answer[0]);
                    } else {
                        System.out.println("Please, type \"y\" (means YES) or \"n\" (means NO).");
                    }
                }
            }
        } else if (!Constants.LIST_OF_COMMANDS.contains(command)) {
            String misleadingMsg = "Command hasn't been found.";
            throw new InvalidCommandException(misleadingMsg);
        }

        switch (command) {
            case Constants.COMMAND_QUIT: {
                terminateProgram();
                break;
            }
            case Constants.COMMAND_LIST: {
                performCommandList();
                break;
            }
            case Constants.COMMAND_PRICE: {
                // SIGNATURE: PRICE <CURRENCY_PAIR>
                if (commandInput.length != Constants.COMMAND_PRICE_ARGS_AMOUNT) {
                    throw new InvalidCommandUsageException(command);
                }
                try {
                    performCommandPrice(commandInput);
                } catch (IllegalArgumentException error) {
                    System.out.println(error.getMessage());
                } catch (Exception error) {
                    System.out.println("An error has occured while performing PRICE command: "
                            + error.getMessage());
                }
                break;
            }
            case (Constants.COMMAND_STOP): {
                performCommandStop();
                break;
            }
            case (Constants.COMMAND_BUY): {
                // SIGNATURE: BUY <amount> <currency>
                if (commandInput.length != Constants.COMMAND_BUY_ARGS_AMOUNT) {
                    throw new InvalidCommandUsageException(command);
                }
                try {
                    performTradingCommand(commandInput);
                } catch (IllegalArgumentException error) {
                    System.out.println(error.getMessage());
                } catch (Exception error) {
                    System.out.println("ERROR: currency couldn't be bought. Reason: " + error.getMessage());
                }
                break;
            }
            case (Constants.COMMAND_SELL): {
                // SIGNATURE: SELL <amount> <currency>
                if (commandInput.length != Constants.COMMAND_BUY_ARGS_AMOUNT) {
                    throw new InvalidCommandUsageException(command);
                }
                try {
                    performTradingCommand(commandInput);
                } catch (IllegalArgumentException error) {
                    System.out.println(error.getMessage());
                } catch (Exception error) {
                    System.out.println("ERROR: currency couldn't be sold. Reason: " + error.getMessage());
                }
                break;
            }
        }
    }
}
