package com.company;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Constants {

    // NOTE: Prompt numerical options

    public static final int AVALIABLE_ARGUMENTS_AMOUNT = 1;
    public static final int EXCHANGE_RATES_UPDATE_TIME_SEC = 1;
    public static final int COMMAND_PRICE_ARGS_AMOUNT = 2;
    public static final int COMMAND_BUY_ARGS_AMOUNT = 3;



    public static final String USAGE_EXAMPLE = "USAGE: java -cp . com.company.CurrencyChecker /path/filename.csv";
    public static final String REQUIRED_EXTERNSION = "csv";
    public static final Character CSV_FILE_DELIMITER = ';';


    public static final int EXIT_SUCCESS = 1;


    // NOTE: PROMPT COMMANDS

    public static final String COMMAND_PRICE = "PRICE";
    public static final String COMMAND_STOP = "STOP";
    public static final String COMMAND_BUY = "BUY";
    public static final String COMMAND_SELL = "SELL";
    public static final String COMMAND_QUIT = "QUIT";
    public static final String COMMAND_LIST = "LIST";
    public static final String ANSWER_YES = "Y";
    public static final String ANSWER_NO = "N";
    public static final String CONFIRM_BUYING = "B";
    public static final String CONFIRM_SELLING = "S";


    public static List<String> LIST_OF_COMMANDS = Stream.of(COMMAND_PRICE, COMMAND_STOP, COMMAND_SELL,
            COMMAND_BUY, COMMAND_QUIT, COMMAND_LIST, ANSWER_YES, ANSWER_NO, CONFIRM_BUYING, CONFIRM_SELLING).collect(Collectors.toList());

    // NOTE: CURRENCY TAGS

    public static final String CURRENCY_TAG_USG = "USD";

    // NOTE: Exception messages
    public static final String FILE_NOT_EXIST_MSG = "File doesn't exists.";

}
