package com.company;

public class InvalidCommandException extends RuntimeException {

    private String message;
    public InvalidCommandException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        String misleadingMsg = message + " Avaliable commands: " + Constants.LIST_OF_COMMANDS;
        return misleadingMsg;
    }
}
