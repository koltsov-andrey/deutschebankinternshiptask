package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandParserTest {

    public static int calculateFactorial(final int N) {
        if(N == 1) {
            return 1;
        }
        return N * calculateFactorial(N - 1);
    }

    @Test
    public void getPermuniation() {
        ArrayList<String> currencyTags = new ArrayList<String>(
                Arrays.asList("EUR", "USD", "RUB", "JPY", "GBP"));
        ArrayList<String> permutations = CommandParser.getPermuniation(currencyTags);
        int amountOfPositionsInCombinationElement = 2;
        int expectedAmountOfElements = calculateFactorial(currencyTags.size()) / calculateFactorial(currencyTags.size() - amountOfPositionsInCombinationElement);
        int actualAmountOfElements = permutations.size();
        assertEquals(expectedAmountOfElements, actualAmountOfElements);
    }


}