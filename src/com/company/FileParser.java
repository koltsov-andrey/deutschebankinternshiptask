package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class FileParser {

    public static Path filePath;
    public static File file;
    public static HashMap<String, List<Double>> currencyData;

    public FileParser(String filePath) throws FileNotFoundException, IllegalArgumentException, IOException {
        Path path = Paths.get(filePath);
        this.filePath = path.toRealPath(LinkOption.NOFOLLOW_LINKS);
        this.file = new File(filePath.toString());
        if (!this.file.exists()) {
            throw new FileNotFoundException(Constants.FILE_NOT_EXIST_MSG);
        }
        this.currencyData = new HashMap<String, List<Double>>();
        if (!isFileExtensionCorrect()) {
            String misleadingMsg = "File extension is not supported. Required extension: " + Constants.REQUIRED_EXTERNSION;
            throw new IllegalArgumentException(misleadingMsg);
        }
    }

    private final boolean isFileExtensionCorrect() {
        String fileExtension = this.getFileExtension(filePath);
        int extensionDifference = fileExtension.compareTo(Constants.REQUIRED_EXTERNSION);
        return (extensionDifference == 0);
    }


    public static final String getFileExtension(Path filePath) {
        String fileName = filePath.getFileName().toString();
        int i = fileName.lastIndexOf('.');
        String fileExtension = "";
        if (i > 0) {
            fileExtension = fileName.substring(i + 1);
        }
        return fileExtension;
    }

    public HashMap<String, List<Double>> getDataFromFile() throws FileNotFoundException, IOException {
        String currentFilePath = this.filePath.toString();
        FileReader fileReader = new FileReader(currentFilePath);
        try (BufferedReader bufReader = new BufferedReader(fileReader)) {
            String line = "";
            while ((line = bufReader.readLine()) != null) {
                String[] currentFileData = line.split((Constants.CSV_FILE_DELIMITER).toString());
                if (Arrays.asList(currentFileData).contains(" ")) {
                    // NOTE: Erasing whitespaces
                    currentFileData = Arrays.stream(currentFileData)
                            .map(s -> s.replace(" ", ""))
                            .toArray(size -> new String[size]);
                }
                if (Arrays.asList(currentFileData).contains("\n")) {
                    // NOTE: changing EOL mark
                    currentFileData = Arrays.stream(currentFileData)
                            .map(s -> s.replace("\n", ";"))
                            .toArray(size -> new String[size]);
                }
                List<Double> currencyValues = new ArrayList<Double>();
                currencyValues.add((Double.parseDouble(currentFileData[1])));
                currencyValues.add((Double.parseDouble(currentFileData[2])));
                this.currencyData.put(currentFileData[0], currencyValues);
            }
            List<Double> usdValues = new ArrayList<Double>();
            usdValues.add(1.0); // Price related to itself
            usdValues.add(0.0); // deviation from itself
            this.currencyData.put(Constants.CURRENCY_TAG_USG, usdValues);
        } catch (NumberFormatException error) {
            String misleadingMsg = "ERROR: value format is not correct";
            System.out.println(misleadingMsg);
        } catch (Error error) {
            String misleadingMessage = "An error has occured while parsing the file.";
            System.out.println(misleadingMessage + error.getMessage());
        }
        return currencyData;
    }
}


