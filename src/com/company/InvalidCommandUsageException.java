package com.company;

public class InvalidCommandUsageException extends RuntimeException {

    private String command;

    public InvalidCommandUsageException(String command) {
        this.command = command;
    }

    @Override
    public String getMessage() {
        String misleadingMsg = "Syntax error for command " + this.command + " USAGE: " + getUsage(this.command);
        return misleadingMsg;
    }

    private String getUsage(String command) {
        String usage = "";
        switch (command) {
            case (Constants.COMMAND_PRICE): {
                usage = "PRICE <currnecy_pair>";
                break;
            }
            case (Constants.COMMAND_SELL): {
                usage = "SELL <amount> <currency>";
                break;
            }
            case (Constants.COMMAND_BUY): {
                usage = "BUY <amount> <currency>";
                break;
            }
        }
        return usage;
    }
}
