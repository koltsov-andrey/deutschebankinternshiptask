package com.company;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ExchangeRatesPrinter implements Runnable {

    private final double priceLowerBound;
    private final double priceUpperBound;
    private double currentPrice;


    private static Thread thread;

    public static boolean isTerminated;

    private String lhsCurrencyTag;
    private String rhsCurrencyTag;

    private static HashMap<String, List<Double>> currencyData;
    public static boolean isActive = false;
    public static boolean isBuying = false;
    public static boolean isSelling = false;

    public ScheduledExecutorService executor;

    public ExchangeRatesPrinter(double priceLowerBound, double priceUpperBound, HashMap<String, List<Double>> currencyData,
                                String lhsCurrencyTag, String rhsCurrencyTag) {
        this.priceLowerBound = priceLowerBound;
        this.priceUpperBound = priceUpperBound;
        this.currencyData = currencyData;
        this.isTerminated = false;
        int currencyValueIndexPosition = 0; // value related to USD
        this.currentPrice = (currencyData.get(lhsCurrencyTag)).get(currencyValueIndexPosition);

        this.lhsCurrencyTag = lhsCurrencyTag;
        this.currentPrice = CommandParser.getPriceOfCurrencyRelatedToUSD(lhsCurrencyTag);
        this.rhsCurrencyTag = rhsCurrencyTag;
        this.isActive = false;
    }

    public double getCurrentPrice() {
        return this.currentPrice;
    }

    private double getPriceRelatedToUsd(double rhsPriceRelatedToUSD) {
        return (this.currentPrice * rhsPriceRelatedToUSD);
    }

    public void pause() {
        this.isActive = false;
    }

    public void activate() {
        this.isActive = true;
    }

    public void terminate() {
        this.isTerminated = true;
        List<Double> lhsCurrencyValues = currencyData.get(lhsCurrencyTag);
        List<Double> rhsCurrencyValues = currencyData.get(rhsCurrencyTag);
        // NOTE: Updating currency value related to USD
        int currencyValueIndexPosition = 0; // value related to USD
        double currentPriceRelatedToUSD = getPriceRelatedToUsd(rhsCurrencyValues.get(currencyValueIndexPosition));
        lhsCurrencyValues.set(currencyValueIndexPosition, currentPriceRelatedToUSD);
        currencyData.put(lhsCurrencyTag, lhsCurrencyValues);
    }


    @Override
    public void run() {
        if (!isTerminated && isActive) {
            Random randomNumberGenerator = new Random();
            this.currentPrice = priceLowerBound + (priceUpperBound - priceLowerBound) * randomNumberGenerator.nextDouble();
            if (this.currentPrice < 0) {
                this.currentPrice = Math.abs(this.currentPrice);
            }
            this.currentPrice = Double.parseDouble(String.format("%.3f", this.currentPrice));

            if (isBuying) {
                String whiteSpace = " ";
                String infoMessage = "  [type \"b\" to confirm]";
                double transactionValue = Double.parseDouble(String.format("%.3f", CommandParser.amountOfPurchase * this.currentPrice));
                System.out.println("BUY " + whiteSpace + CommandParser.amountOfPurchase + whiteSpace + CommandParser.currentCurrency + " FOR " +
                        transactionValue + whiteSpace + CommandParser.relatedCurrency + infoMessage);
            } else if (isSelling) {
                double transactionValue = Double.parseDouble(String.format("%.3f", CommandParser.amountOfPurchase * (1 / this.currentPrice)));
                String whiteSpace = " ";
                String infoMessage = "  [type \"s\" to confirm]";
                System.out.println("SELL " + whiteSpace + CommandParser.amountOfPurchase + whiteSpace + CommandParser.relatedCurrency + " FOR " +
                        transactionValue + whiteSpace + CommandParser.currentCurrency + infoMessage);

            } else {
                System.out.println(this.currentPrice);
            }
        }
    }

    public void start() {
        this.isActive = true;
        if (thread == null) {
            thread = new Thread(this);
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            executor.scheduleAtFixedRate(this, 0, Constants.EXCHANGE_RATES_UPDATE_TIME_SEC, TimeUnit.SECONDS);
            thread.start();
        }
    }
}
