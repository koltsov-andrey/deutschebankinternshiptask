package com.company;

import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileParserTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

   @Test
    public void processFile() throws IOException {
        exception.expect(NoSuchFileException.class);
        FileParser parser = null;
        String exampleFilePath = "User/root.csv";
        parser = new FileParser(exampleFilePath);
    }

    @Test
    public void getFileExtension() {
       String excpectedExtesion = "txt";
       String fileName = "blablabla." + excpectedExtesion;
       Path path = Paths.get(fileName);
       String extension = FileParser.getFileExtension(path);
        assertEquals(extension, excpectedExtesion);
    }
}
